package com.groupeIsi.msindus;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MsIndusApplication {

	public static void main(String[] args) {
		SpringApplication.run(MsIndusApplication.class, args);
	}

}
